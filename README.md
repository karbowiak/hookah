# Hookah

\>> insert image of fancy artistry hookah with smoke around it <<

# Prerequesites
1. Working PHP8+
2. Composer installed on system
# Installation
1. Clone to ~/.githooks
2. Install dependencies with `composer install -o`
3. Run `git config --global core.hooksPath ~/.githooks/git` to set the global githooks directory
4. Copy the `.hookah` file to your project, set it up and voila.
