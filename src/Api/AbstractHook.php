<?php

namespace Hookah\Api;

use Hookah\Helpers\Config;
use League\CLImate\CLImate;

abstract class AbstractHook
{
    protected CLIMate $cli;
    protected Config $config;
    protected int $sortOrder = 0;

    abstract public function handle();

    public function __construct()
    {
        $this->cli = new \League\CLImate\CLImate();
        $this->config = new Config();
    }

    public function success(): void
    {
        exit(0);
    }

    public function fail(?string $reason = null): void
    {
        if ($reason !== null) {
            $this->cli->error($reason);
        }

        exit(1);
    }

    public function ask(
        string $question
    ): string {
        $this->cli->out($question);
        return trim(exec('exec < /dev/tty && read input && echo $input'));
    }

    public function askWithConfirmation(
        string $question,
        ?string $proceedAnswer = null
    ): bool {
        $this->cli->out($question);
        $result = trim(exec('exec < /dev/tty && read input && echo $input'));
        return strtolower($result) === strtolower($proceedAnswer);
    }

    public function getCurrentBranch(): string
    {
        return trim(exec('git rev-parse --abbrev-ref HEAD'));
    }

    public function isMergeOrRebase(): bool
    {
        $isMerge = false;

        // If we're in a merge, don't run this
        $mergeHash = $this->exec('git rev-parse -q --verify MERGE_HEAD');
        $rebaseHash = $this->exec('git rev-parse -q --verify REBASE_HEAD');
        if (!empty($mergeHash) || !empty($rebaseHash)) {
            $isMerge = true;
        }

        return $isMerge;
    }

    public function exec(string $exec, bool $shell = false): string
    {
        if ($shell === true) {
            return trim(shell_exec($exec));
        }
        return trim(exec($exec));
    }
}
