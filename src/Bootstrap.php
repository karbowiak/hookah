<?php

namespace Hookah;

use Kcs\ClassFinder\Finder\ComposerFinder;
use League\Container\Container;
use League\Container\ReflectionContainer;

class Bootstrap
{
    protected $types = [
        'applypatch-msg' => 'Hookah\Hooks\ApplyPatchMsg',
        'commit-msg' => 'Hookah\Hooks\CommitMsg',
        'post-applypatch' => 'Hookah\Hooks\PostApplyPatch',
        'post-checkout' => 'Hookah\Hooks\PostCheckout',
        'post-commit' => 'Hookah\Hooks\PostCommit',
        'post-merge' => 'Hookah\Hooks\PostMerge',
        'post-receive' => 'Hookah\Hooks\PostReceive',
        'post-rewrite' => 'Hookah\Hooks\PostRewrite',
        'post-update' => 'Hookah\Hooks\PostUpdate',
        'pre-applypatch' => 'Hookah\Hooks\PreApplyPatch',
        'pre-auto-gc' => 'Hookah\Hooks\PreAutoGC',
        'pre-commit' => 'Hookah\Hooks\PreCommit',
        'pre-push' => 'Hookah\Hooks\PrePush',
        'pre-rebase' => 'Hookah\Hooks\PreRebase',
        'pre-receive' => 'Hookah\Hooks\PreReceive',
        'prepare-commit-msg' => 'Hookah\Hooks\PrepareCommitMsg',
        'update' => 'Hookah\Hooks\Update'
    ];

    protected Container $container;

    public function __construct()
    {
        $this->buildContainer();
    }

    public function run(array $argv)
    {
        $taskFileName = str_replace('git/', '', $argv[0]);
        $taskClass = $this->types[$taskFileName];

        $taskFinder = new ComposerFinder();
        $taskFinder->inNamespace($taskClass);

        foreach ($taskFinder as $className => $reflection) {
            /** @var \Hookah\Api\AbstractHook $instance */
            $instance = $this->container->get($className);
            $instance->handle();
        }
    }

    protected function buildContainer(): void
    {
        $this->container = new Container();

        $this->container->delegate(
            new ReflectionContainer()
        );
    }
}
