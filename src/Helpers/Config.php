<?php

namespace Hookah\Helpers;

use Symfony\Component\Yaml\Yaml;
use Tightenco\Collect\Support\Collection;

class Config
{
    protected Collection $config;

    public function __construct()
    {
        $configLocation = getcwd() . '/.hookah.yaml';
        if (!file_exists($configLocation)) {
            exit(0);
        }

        $data = Yaml::parseFile($configLocation);
        $this->config = new Collection($data);
    }

    public function get(string $key, ?string $default = null)
    {
        try {
            if (strpos($key, '/') !== false) {
                $cnt = 1;
                $keys = explode('/', $key);
                $keyCount = count($keys) - 1; // We already used the first key
                if ($this->config === null) {
                    return $default;
                }
                $selectedItem = $this->config->get($keys[ 0 ]);
                do {
                    $selectedItem = $selectedItem[ $keys[ $cnt ] ];
                    $cnt++;
                } while ($cnt <= $keyCount);

                return $selectedItem;
            }
            if (!$this->config->has($key)) {
                return $default;
            }

            return $this->config->get($key);
        } catch (\Exception $e) {
            return $default;
        }
    }

    public function all(): array
    {
        return $this->config->all();
    }
}
