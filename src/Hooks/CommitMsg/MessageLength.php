<?php

namespace Hookah\Hooks\CommitMsg;

use Hookah\Api\AbstractHook;

class MessageLength extends AbstractHook
{
    public function handle(): void
    {
        $enabled = $this->config->get('messageLength/enabled', false);
        if ($enabled === false) {
            return;
        }

        if ($this->isMergeOrRebase()) {
            return;
        }

        $messageWarningLength = $this->config->get('messageLength/warningCharacters');
        $messageMaxLength = $this->config->get('messageLength/maxCharacters');
        $commitMessageFile = $_SERVER[ 'argv' ][ 1 ] ?? null;
        $commitMessageFilePath = \getcwd() . '/.git/' . $commitMessageFile; // @TODO verify this works ?
        dd($commitMessageFilePath);
        $commitMessage = $commitMessageFile === null ? '' : $this->exec("cat {$commitMessageFilePath} | grep -v '#'", true);
        dd($commitMessage);
        $commitMessageLength = strlen($commitMessage);

        if ($commitMessageLength > $messageWarningLength && $commitMessageLength < $messageMaxLength) {
            $this->cli->out('<red>WARNING!</red> Your commit message is more than <green>' . $messageWarningLength . '</green> characters long');
            $proceed = $this->askWithConfirmation('Want to continue? y/N', 'n');
            if ($proceed) {
                $this->fail('Commit was cancelled!');
            }
        }

        if ($commitMessageLength > $messageMaxLength) {
            $this->cli->out('<red>WARNING!</red> Your commit message is more than than <green>' . $messageMaxLength . '</green> characters long');
            $proceed = $this->askWithConfirmation('<yellow>ARE YOU REALLY SURE?</yellow> yesiamsure/N', 'yesiamsure');
            if ($proceed) {
                $this->fail('Commit was cancelled!');
            }
        }
    }
}
