<?php

namespace Hookah\Hooks\CommitMsg;

use Hookah\Api\AbstractHook;

class TaskPrefix extends AbstractHook
{
    public function handle(): void
    {
        if (empty($this->config->get('taskPrefix/enabled', false))) {
            return;
        }

        $taskPrefix = $this->config->get('taskPrefix/prefix');
        $currentBranch = $this->getCurrentBranch();
        $commitMessageFile = $_SERVER['argv'][1];
        $commitMessage = file_get_contents(getcwd() . '/' . $commitMessageFile);

        // Find task id from branch
        preg_match("/({$taskPrefix}-\d+)/", $currentBranch, $taskId);
        if (empty($taskId)) {
            $this->cli->error('WARNING! Unable to detect task id from branch!');
            $proceed = $this->askWithConfirmation('Do you want to ignore it, and commit anyway? (y/N)', 'n');

            if ($proceed) {
                $this->fail('Commit was cancelled!');
            }
        }

        // preg_match finds two results - take the first one
        $taskId = @$taskId[0];

        // Check if commit message contains task id matching branch name, if not - add it
        preg_match("/(\[{$taskId}]\s.+|.*Merge.*)/", $commitMessage, $taskMsg);
        if (empty($taskMsg)) {
            preg_match('/(^\[.*])/', $commitMessage, $msgTask);
            if (!empty($msgTask)) {
                $this->cli->out('Your commit message seem to contain a wrong task id: ' . $msgTask[0]);
                $proceed = $this->askWithConfirmation('Do you want to replace it with task id: ' . $taskId . ' (Y/n)', 'y');

                if ($proceed) {
                    $commitMessage = str_replace($msgTask[0], '', $commitMessage);
                    $commitMessage = "[{$taskId}] {$commitMessage}";
                    file_put_contents($commitMessageFile, $commitMessage);
                    $this->success();
                }
            }

            $this->cli->error('WARNING! No task id found');
            $proceed = $this->askWithConfirmation('Prepend task id? (Y/n)', 'y');
            if ($proceed) {
                $commitMessage = "[{$taskId}] {$commitMessage}";
                file_put_contents($commitMessageFile, $commitMessage);
                $this->success();
            }
        }
    }
}
