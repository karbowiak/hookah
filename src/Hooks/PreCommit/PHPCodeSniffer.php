<?php

namespace Hookah\Hooks\PreCommit;

use Hookah\Api\AbstractHook;

class PHPCodeSniffer extends AbstractHook
{
    public function handle()
    {
        if (empty($this->config->get('phpCodeSniffer/enabled', false))) {
            return;
        }

        // Find the paths to the files required to run all of this
        $phpcs = $this->exec('which phpcs');
        $phpcbf = $this->exec('which phpcbf');
        $composer = $this->exec('which composer');

        // If we're missing composer, it's all over..
        if (empty($composer)) {
            $this->cli->error('WARNING! composer is not installed on your system, please install with your package manager');
            $this->fail();
        }

        // Find out if phpcs and phpcbf is on the system
        if (empty($phpcs) || empty($phpcbf)) {
            $this->cli->error('WARNING! phpcs and phpcbf is missing on the system, please install with: composer global require squizlabs/php_codesniffer');
            $this->fail();
        }

        // Run the codesniffer
        $memoryLimit = $this->config->get('phpCodeSniffer/memoryLimit', '1G');
        $errorSeverity = $this->config->get('phpCodeSniffer/errorSeverity', '1');
        $warningSeverity = $this->config->get('phpCodeSniffer/warningSeverity', '1');
        $codingStandard = $this->config->get('phpCodeSniffer/codingStandard', 'PSR2');
        $paths = explode(' ', $this->exec('git diff --cached --name-only --diff-filter=ACM -z | xargs -0'));
        $this->cli->out('Running codesniffer on ' . count($paths) . ' path(s)');
        foreach ($paths as $path) {
            $path = getcwd() . '/' . $path;
            // If the file exists, and it ends in .php we'll codesniff it..
            if (file_exists($path) && stripos($path, '.php') !== false) {
                $shellCommand = "{$phpcs} -d memory_limit={$memoryLimit} --standard={$codingStandard} --error-severity={$errorSeverity} --warning-severity={$warningSeverity} {$path}";
                $sniffer = $this->exec($shellCommand, true);
                if (!empty($sniffer)) {
                    $this->cli->out($sniffer);
                    $this->cli->error('WARNING! You have problems with the codesniffer');
                    $this->fail();
                }
            }
        }
    }
}
