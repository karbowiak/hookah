<?php

namespace Hookah\Hooks\PrePush;

use Hookah\Api\AbstractHook;

class ProtectBranches extends AbstractHook
{
    public function handle(): void
    {
        if (empty($this->config->get('protected/enabled', false))) {
            return;
        }

        $branch = $this->getCurrentBranch();
        $pushCommand = $this->exec('ps -ocommand= -p ' . posix_getppid());

        $protectedBranches = $this->config->get('protected/branches');
        $forcePush = ['force', 'delete', '-f'];

        if (in_array($branch, $protectedBranches, true) || (in_array($branch, $protectedBranches, true) && str_ireplace($forcePush, '', $pushCommand) !== $pushCommand)) {
            $this->cli->flank("WARNING! You are trying to push to a protected branch: {$branch}", '/!\\');
            $proceed = $this->askWithConfirmation('ARE YOU REALLY SURE? (yesiamsure/N)', 'yesiamsure');
            if (!$proceed) {
                $this->fail();
            }
        }
    }
}
