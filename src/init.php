<?php

# Init composer

use Hookah\Bootstrap;

$autoloaderPath = dirname(__DIR__, 1) . '/vendor/autoload.php';
if (!file_exists($autoloaderPath)) {
    throw new RuntimeException('Error, composer is not setup correctly.. Please run composer install');
}
$autoloader = require $autoloaderPath;

# Bootstrap the system
return [new Bootstrap($autoloader), $autoloader];
